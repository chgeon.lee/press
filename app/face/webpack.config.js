const path = require("path");
const DotenvWebpack = require("dotenv-webpack");

module.exports = (env) => {
  return {
    entry: {
      main: path.resolve(__dirname, "src", "index.tsx"),
    },
    output: {
      filename: "bundle.js",
      path: path.resolve(__dirname, "dist"),
      publicPath: "/",
    },
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          use: "ts-loader",
          exclude: /node_modules/,
        },
        {
          test: /\.scss$/,
          use: ["style-loader", "css-loader", "sass-loader"],
        },
        {
          test: /\.css$/,
          use: ["style-loader", "css-loader"],
        },
        {
          test: /\.(jpg|jpeg|gif|png|svg|ico)?$/,
          use: ["file-loader"],
        },
      ],
    },
    resolve: {
      extensions: [".tsx", ".ts", ".js"],
      alias: {
        "@": path.resolve(__dirname, "../../"),
      },
    },
    plugins: [
      new DotenvWebpack({
        path:
          env.mode == "development"
            ? "./.env/.env.development"
            : env.mode == "production"
            ? "./.env/.env.production"
            : "./.env/.env.local",
      }),
    ],
    performance: {
      hints: process.env.NODE_ENV === "production" ? "warning" : false,
      maxAssetSize: 300000,
      maxEntrypointSize: 500000,
    },
    devServer: {
      static: path.join(__dirname, "dist"),
      historyApiFallback: true,
      compress: true,
      port: 9000,
    },
  };
};
