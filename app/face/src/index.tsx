import React from "react";
import CreateDOM from "react-dom/client";

const rootElement = document.getElementById("root");
const root = CreateDOM.createRoot(rootElement);

const element = <div>hello</div>;

root.render(element);
